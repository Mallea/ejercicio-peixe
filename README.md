# Ejercicio1

Este proyecto ha sido generado con la version de [Angular CLI](https://github.com/angular/angular-cli)  8.3.20.

# Instrucciones

1. Clonar proyecto (git clone https://Mallea@bitbucket.org/Mallea/ejercicio-peixe.git)
2. Moverse a rama develop (git checkout develop)
3. Ejecutar comando "npm install" para instalar librerías
4. Ejecutar comando "ng serve"
5. Navegar a http://localhost:4200/

